import os
import subprocess

output_base = "./output"
os.makedirs(output_base, exist_ok=True)


def recusive_copy(current_directory: str = ""):

    if not current_directory:
        hfs_directory = ':*'
        output_directory = output_base + ""
    else:
        hfs_directory = f"{current_directory}:*"
        # mac os uses : to seperate directories. It's an illegal character in mac os, so it's fine to convert.
        # however slashes are legal so this will not always work.
        os.makedirs(f"{output_base}{current_directory.replace(':', '/')}", exist_ok=True)
        output_directory = f"{output_base}/{current_directory.replace(':', '/')}"

    print(f"Copying {hfs_directory} to {output_directory}")

    process = subprocess.Popen(["sudo", "hcopy", "-r", hfs_directory, output_directory], stderr=subprocess.PIPE)
    output, error = process.communicate()
    # print(f"error: {error}")

    # https://docs.python.org/2.4/lib/standard-encodings.html
    decoded = error.decode("mac_roman")
    for line in decoded.split('\n'):
        if "is a directory" in line:
            # this is absolute vomit code, but it works.
            line = line.replace('hcopy: "', '')
            directory = line.replace('": is a directory', '')
            print(f"Going to directory: {directory}")
            recusive_copy(directory)


if __name__ == '__main__':
    recusive_copy()
