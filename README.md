# About
This is a half baked script that adds recursive copy abilities to hcopy.

Hcopy is a part of hfsutils, which is used to mount and explore classic mac os volumes.

The tools are here, and should be installed with brew: brew install hfsutils.
https://linux.die.net/man/1/hfsutils

After that, you can use this command to copy the entire contents of a cd or volume automatically.

This requires the brew command and some skill with the Mac OS Terminal to run.
This tool is not supported, you're pretty much on your own, but here is what i did to get it working.


## To get this to work:
### 1: insert the CD
### 2: find the mountpoint: 

Run "diskutil list"

-> Note that there is a "Apple_HFS" in the list below

    Example:
    /dev/disk2 (internal, physical):
       #:                       TYPE NAME                    SIZE       IDENTIFIER
       0:        CD_partition_scheme                        *576.6 MB   disk2
       1:     Apple_partition_scheme                         502.1 MB   disk2s1
       2:        Apple_partition_map                         1.0 KB     disk2s1s1
       3:                  Apple_HFS Mac • CD                501.8 MB   disk2s1s2


# 3: mount the Apple_HFS disk: 

Run: "sudo hmount /dev/disk2s1s2" <- Use the proper disk name here.

    Volume name is "Mac ? CD" (locked)
    Volume was created on Sat Nov 12 18:59:13 1994
    Volume was last modified on Sun Nov 13 10:55:32 1994
    Volume has 36264960 bytes free

# 4: Verify the disk is mounted

By listing the root directory, run: sudo hls

    ?????????       ??????? ?       ?????? ??       ??????  ?       ????? ???       ------>         Desktop Folder  Games           Music
    ????????        ???????         ?????? ?        ??????          ????? ??        Comm. Software  Fonts           MAC ? CD        Trash


# 5: Test copying:

Run: sudo hcopy -r ":*" 

Now you can hcopy whatever you want file by file, but that's not what we need. If you DO need it, look at the
hfsutils commands documentation to get that one file you need.

Example output:

    sudo hcopy -r ":*" output_directory
    hcopy: ":------>": is a directory
    hcopy: ":Comm. Software": is a directory
    hcopy: ":Desktop Folder": is a directory
    hcopy: ":Fonts": is a directory
    hcopy: ":Games": is a directory
    hcopy: ":MAC ? CD": error opening destination file (Illegal byte sequence)
    hcopy: ":Music": is a directory
    hcopy: ":Trash": is a directory

# 6: Background research about capabilities of hcopy
There is nothing in the manual about including directories:

* see: https://linux.die.net/man/1/hcopy
* see: http://www.matthewhughes.co.uk/how-to-mount-hfs-classic-drives-on-macos-catalina-and-later/
* see: https://swissmacuser.ch/hfs-volume-data-recovery-diskutility-could-not-mount-error-49153/

# 7: Hcopy everything recusively using python
So that's where you can use this tool.

You need python3: brew install python3. You want python 3.7 or higher, which is the default.

Run: python3 hfsdiskrecursivecopy.py

# 8: Sit back and relax
It will make an "output" directory and will basically dump anything in there recusively.

You might want to modify this script to use hcopy's interpretation flags, such as -r or -m.
Good luck with that: it's a guessing game what works best. I wanted to retrieve some MOD files
because of memories.

Caveats are directories or files with a / in their name might not be copied and error. This should be
replaced in the output dir, but have not encountered any errors yet during recovery for the files i needed.

# 9: Thanks to hfsutils authors
Thanks to the creators of hfsutils, it's great and because it's so stable in output i could whip up this script
quickly and efficiently. I've been most happy with the results.

# 10: Remove the disk

humount
